# Data Scientist Technical Test

Welcome to Digital Fineprint Data Scientist Technical Test repository. In this repository 
you will have access to the technical test pdf and the raw data required for the machine learning 
module.


### Task 
1. Please create a fork of this repository on Bitbucket and work on the local fork.
[Guide to fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
2. Read through the test questions in the pdf and complete the assignments accordingly.
3. Commit and push all the source code and notebook into the *src* folder under your forked repository.
4. Once you have completed the test, please drop an email to your contact in Digital Fineprint
 along with the slides. We will review the slides along with the code and get back to you shortly.


### Tips
When completing the tasks, please also think about
- Performance in I/O operation
- Readability of the code
- Reusability of the code 


If you have any questions related to the test, please feel free to drop an email to us and we will
response as soon as we can. Good luck!